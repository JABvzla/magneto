module.exports = {
  preset: 'ts-jest',
  rootDir: '.',
  collectCoverageFrom: [
    'tests/**/*.ts',
    '!tests/**/*.d.ts',
    '!tests/**/*.spec.ts',
    '!tests/**/*.test.ts',
    '!tests/**/__*__/*',
  ],
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js"
  ],
  testEnvironment: 'node'
};
