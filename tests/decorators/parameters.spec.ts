import * as supertest from 'supertest'
import TestsController from '../TestsController';

const app = TestsController.express;

describe("Parameters", () => {
  describe("QueryParams Decorator", () => {
    it("should be get all queryParameters", async () => {
      const queryParameters = '?name=john&surname=wick';
      const expectedResult = {"name": "john", "surname": "wick"};

      const result = await supertest(app).get(`/api/tests/get-query-params${queryParameters}`);

      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual(expectedResult);
    });


    it("should be transform to array when key params repeat", async () => {
      const queryParameters = '?job=task1&job=task2';
      const expectedResult = { "job": ["task1", "task2"] };

      const result = await supertest(app).get(`/api/tests/get-query-params${queryParameters}`);

      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual(expectedResult);
    });
  });

  describe("Body Decorator", () => {
    it("should be get all body", async () => {
      const body = '?name=john&surname=wick';
      const expectedResult = {"name": "john", "surname": "wick"};

      const result = await supertest(app).get(`/api/tests/get-query-params${body}`);

      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual(expectedResult);
    });


    it("should be transform to array when key params repeat", async () => {
      const body = { name: "john", lastName: "wick" };
      const expectedResult = body;

      const result: any = await supertest(app).post('/api/tests/post-body').send(body);
      

      expect(result.status).toEqual(200);
      // TODO: fix this assert
      // expect(result).toEqual(expectedResult);
    });
  });
});
