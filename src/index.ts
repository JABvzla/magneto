import app from './core/express';
import { Request, Response, NextFunction, Router } from 'express';
import TestController from './controllers/TestController';

// Examples: How to use it!

// Use it with an array
app.middleware([firstMiddleware, secondMiddleware])

function firstMiddleware(req: Request, _: Response, next: NextFunction) {
    console.log(req.headers)
    console.log('middleware');
    next();
}

function secondMiddleware(req: Request, _: Response, next: NextFunction) {
    console.log((req as any).context)
    console.log('other middleware');
    next();
}

// Use it with a callback
// app.middleware((req: Request, _: Response, next: NextFunction) => {
//     console.log('current middleware');
//     console.log((req as any).context.country);
//     next();
// })

// Use it with a function
app.middleware(withOutParams())

function withOutParams() {
    return (req: Request, _: Response, next: NextFunction) => {
        (req as any).something = 'this is something';
        console.log('without params:', (req as any).something);
        next();
    }
}

const test = new TestController('gaby', 'rivasssss');
const router: Router = Router();

(test as any).setRouter(router);

app.start();
