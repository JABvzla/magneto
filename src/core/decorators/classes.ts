import { Router } from 'express';
import app from '../express';
import { APP_ROUTER } from './methods';

interface IConfigController {
    path: string
}

export function Controller(config?: IConfigController) {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        let path = constructor.name.toLowerCase().replace(/controller/, '');
        if (config && config.path) {
            path = config.path;
        }

        return class extends constructor {

            public get router() {
                return APP_ROUTER;
            }

            public setRouter(router?: Router): void {
                let routes = constructor.prototype.router;
                const healthcheck = require('express-healthcheck')(); // tslint:disable-line
                routes.get('/health', healthcheck)

                if (router) {
                    routes = [routes, router]
                }

                app.express.use(`/api/${path}`, routes)
            };
        }
    }
}
