import { Router, Request, Response, NextFunction } from 'express';
import { QUERYPARAM_KEY, PARAMETER_KEY } from './parameters';

export let APP_ROUTER: Router;

enum HttpVerb {
    GET = 'get',
    POST = 'post',
    PUT = 'put'
}

function method(path: string, httpVerb: HttpVerb) {
    return (target: any, propertyName: string, descriptor: PropertyDescriptor) => {
        console.log('=== target ===', target)
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (!descriptor) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyName) as PropertyDescriptor;
        }

        const originalMethod = descriptor.value;

        if (!target.hasOwnProperty('router')) {
            target.router = Router();
        }

        APP_ROUTER = target.router;

        const queryParams: number[] = Reflect.getOwnMetadata(QUERYPARAM_KEY, target, propertyName);
        const params: Array<{ idx: number, name: string }> = Reflect.getOwnMetadata(PARAMETER_KEY, target, propertyName);

        const callback = function (req: Request, res: Response, next: NextFunction) {
            const args = []
            if (queryParams && queryParams.length) {
                if (!req.query) {
                    throw new Error('Missing required argument.');
                }

                for (const queryParamIdx of queryParams) {
                    args[queryParamIdx] = req.query;
                }
            }

            if (params && params.length) {
                if (!req.params) {
                    throw new Error('Missing required argument.');
                }

                for (const param of params) {
                    args[param.idx] = req.params[param.name];
                }
            }

            const response = originalMethod.apply(target, args);
            res.json(response);
        }

        descriptor.value = APP_ROUTER[httpVerb](path, callback);
        return descriptor;
    }
}

export function Get(path: string) {return method(path, HttpVerb.GET);}
export const Post = (path: string) => method(path, HttpVerb.POST);
export const Put = (path: string) => method(path, HttpVerb.PUT);
