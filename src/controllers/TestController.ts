import { Controller, Get, QueryParams, Param } from '../core/decorators';

@Controller({ path: 'my-endpoint' })
export default class TestController {
    public name: string;
    public surname: string;

    constructor(name: string, surname: string) {
        this.name = name;
        this.surname = surname;
    }

    @Get('/prueba')
    public getTest(@QueryParams queryParams: any) {
        return {
            some: 'thing',
            success: 'ok',
            queryParams,
        }
    }

    @Get('/prueba/:id')
    public getTestById(@QueryParams queryParams: any, @Param('id') algo: any) {
        console.log('== this ==', this.metodo())
        console.log('== this ==', this.surname);
        return {
            some: 'thing',
            success: 'ok',
            queryParams,
            param: algo,
            constructor: this.metodo()
        }
    }

    private metodo() {
        return 'hola metodo';
    }
}